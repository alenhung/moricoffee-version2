<?php
  //載入網站基本設定
  require '../include/config.php';
  require '../view/tp_siteHeader.php';
  require '../view/tp_header.php';
?>
<?php
  require '../siteAdmin/include/connect/DB_connect.php';
  require '../siteAdmin/include/do_function.php';
  $productTagsTB    =     'productTags';
  $productTB        =     'product';
  $str = "SELECT * FROM $productTB WHERE id='$product_item'";
  $result = mysql_query($str,$link_ID);
  $list = mysql_fetch_array($result);
?>
<div class="container">
  <div  class="thumbnail">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
        
          <img src="<?php echo $list['productImage'] ;?>" alt="" class="img-responsive">
      </div>
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
      <p class="pull-left"><span class="siteContentTitle"><?php echo $list['productName'];?></span></p>
      <p class="pull-right"><span class="productPrice">售價：$<?php echo $list['price'];?></span></p>
       <div class="clearfix"></div>

         <p class="productDesprition">
          <?php echo nl2br($list['description']);?>
         </p>
         <button type="submit" class="btn btn-default">Add to Cart</button>
         <p class="pull-right productDesprition">Tags: 
          <?php
                $tages = $list['tags'];
                $str2 = "SELECT * FROM $productTagsTB WHERE id IN ($tages)";
                $result2 = mysql_query($str2,$link_ID);
                while ($list2 = mysql_fetch_array($result2)) {
                  ?>
                  <button class="btn btn-default btn-xs">
                  <?php
                    echo $list2['tagName'];
                  ?>
                  </button>
              <?php    
                }
              ?>
         </p>
      </div>
    </div>
    <div id="others">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <p class="siteContentTitle">You may like these…</p>
          <hr class="siteContentTitleHR2">
        </div>
        <?php require 'makerList.php';?> 
      </div>
    </div>
   
  </div>
</div>
<?php
  require '../view/tp_footer.php';
  require '../view/tp_siteFooter.php';
?>