<?php
  //載入網站基本設定
  require 'include/config.php';
  require 'view/tp_siteHeader.php';
  require 'view/tp_header.php';
?>
<script src="javascripts/holder/holder.js"></script>
<div class="container">
  <div  class="thumbnail">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
        
          <img data-src="holder.js/460x300/social" alt="" class="img-responsive">
      </div>
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
      <p class="pull-left"><span class="siteContentTitle">Product Name</span></p>
      <p class="pull-right"><span class="productPrice">$price</span></p>
       <div class="clearfix"></div>

         <p class="productDesprition">Like surprise parties, camping trips and the supplest of beef briskets, cold brewed coffee is best executed with a bit of foresight. The Filtron method is, for our money, the most reliable and delicious way to get complex single origin iced coffee at home. It takes about 12 hours overall, but the rewards for a little patience are many: If you, like us, have ever stepped onto a forebodingly hot New York street at 7:13 a.m., groggy but determined, you know that each sip is its own reward.</p>
         <button type="submit" class="btn btn-default">Add to Cart</button>
         <p class="pull-right productDesprition">Tage: <a href="#">tage1</a>,<a href="#">tage1</a>,<a href="#">tage1</a></p>
      </div>
    </div>
    <div id="others">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <p class="siteContentTitle">You may like these…</p>
          <hr class="siteContentTitleHR2">
        </div>
      </div>
    </div>
    <!---->
    <div class="row">
      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
        <div class="productItem">

          <img data-src="holder.js/254x167/social" alt="" class="img-responsive">
          <div class="productItemwords">
            <p class="pull-left">product</p>
            <p class="pull-right"><span class="productPrice">$price</span></p>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
        <div class="productItem">

          <img data-src="holder.js/254x167/social" alt="" class="img-responsive">
          <p class="pull-left">product</p>
          <p class="pull-right"><span class="productPrice">$price</span></p>
        </div>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
        <div class="productItem">

          <img data-src="holder.js/254x167/social" alt="" class="img-responsive">
          <p class="pull-left">product</p>
          <p class="pull-right"><span class="productPrice">$price</span></p>
        </div>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
        <div class="productItem">

          <img data-src="holder.js/254x167/social" alt="" class="img-responsive">
          <p class="pull-left">product</p>
          <p class="pull-right"><span class="productPrice">$price</span></p>
        </div>
      </div>

    </div>
    <!---->
     <!---->
    <div class="row">
      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
        <div class="productItem">
          <img data-src="holder.js/254x167/social" alt="" class="img-responsive">
          <div class="productItemwords">
            <p class="pull-left">product</p>
            <p class="pull-right"><span class="productPrice">$price</span></p>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
        <div class="productItem">
          <img data-src="holder.js/254x167/social" alt="" class="img-responsive">
          <p class="pull-left">product</p>
          <p class="pull-right"><span class="productPrice">$price</span></p>
        </div>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
        <div class="productItem">
          <img data-src="holder.js/254x167/social" alt="" class="img-responsive">
          <p class="pull-left">product</p>
          <p class="pull-right"><span class="productPrice">$price</span></p>
        </div>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
        <div class="productItem">
          <img data-src="holder.js/254x167/social" alt="" class="img-responsive">
          <p class="pull-left">product</p>
          <p class="pull-right"><span class="productPrice">$price</span></p>
        </div>
      </div>
    </div>
    <!---->
  </div>
</div>
<?php
  require 'view/tp_footer.php';
  require 'view/tp_siteFooter.php';
?>