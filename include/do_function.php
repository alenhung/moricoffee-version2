<?php
  // 將 GET 參數轉成同名變數, 例如 $_GET['abc'] => $abc
  if(count($_GET) > 0)
    foreach($_GET  as $k => $v)
      if(is_array($v))            // 若參數本身也是陣列
        foreach($v as $vk => $vv) // 則建立陣列變數
          ${$k}[$vk] = mysql_escape_string($vv);
      else $$k = mysql_escape_string($v);

  // 將 POST 參數轉成同名變數, 例如 $_POST['abc'] => $abc
  if(count($_POST) > 0)
    foreach($_POST as $k => $v)
      if(is_array($v)) 
        foreach($v as $vk => $vv) 
          ${$k}[$vk] = mysql_escape_string($vv);        
      else $$k = mysql_escape_string($v);

?>