<?php
  //載入網站基本設定
  require 'include/config.php';
  require 'view/tp_siteHeader.php';
  require 'view/tp_header.php';

  require 'siteAdmin/include/connect/DB_connect.php';
  require 'siteAdmin/include/do_function.php';
?>

<script src="javascripts/holder/holder.js"></script>
<div class="container">
  <div class="thumbnail">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <p class="text-center"><span class="siteContentTitle">你喜歡哪種味道的喝法？</span></p>
        <hr class="siteContentTitleHR">
        <ul id="coffeeMakerMenu" class="list-inline">
          <li class="machineListImage"><img src="<?php echo SITE_ROOT;?>img/coffieeKind1.gif" alt="" class="img-responsive"></li>
          <li class="machineListImage"><img src="<?php echo SITE_ROOT;?>img/coffieeKind2.gif" alt="" class="img-responsive"></li>
          <li class="machineListImage"><img src="<?php echo SITE_ROOT;?>img/coffieeKind3.gif" alt="" class="img-responsive"></li>
          <li class="machineListImage"><img src="<?php echo SITE_ROOT;?>img/coffieeKind4.gif" alt="" class="img-responsive"></li>
          <li class="machineListImage"><img src="<?php echo SITE_ROOT;?>img/coffieeKind5.gif" alt="" class="img-responsive"></li>
          <li class="machineListImage"><img src="<?php echo SITE_ROOT;?>img/coffieeKind6.gif" alt="" class="img-responsive"></li>
          <li class="machineListImage"><img src="<?php echo SITE_ROOT;?>img/coffieeKind7.gif" alt="" class="img-responsive"></li>
          <li class="machineListImage"><img src="<?php echo SITE_ROOT;?>img/coffieeKind8.gif" alt="" class="img-responsive"></li>
          <li class="machineListImage"><img src="<?php echo SITE_ROOT;?>img/coffieeKind9.gif" alt="" class="img-responsive"></li>
        </ul>
        <hr class="siteContentTitleHR2">
      </div>
    </div>
    <!---->
    <div class="row">
      <?php require 'makers/makerList.php';?> 
    </div>
  </div>
</div>
<?php
  require 'view/tp_footer.php';
  require 'view/tp_siteFooter.php';
?>
