<?php
  session_start();
  //載入網站基本設定
  require 'include/config.php';
  require 'include/check_session.php';
  require 'view/tp_siteHeader.php';
  require 'view/tp_header.php';
?>
<div class="container">
  <div class="row">
  <!--  -->
    <div class="col-lg-3 col-md-3">
      <div class="thumbnail">
        <div class="caption">
          <h3>首頁圖片修訂</h3>
          <p>網站首頁大圖新增修改</p>
          <hr> 
          <p><a href="<?php echo SITE_ADMIN_ROOT;?>introImage/add_introImage.php" class="btn btn-warning" role="button">新增內容</a> <a href="<?php echo SITE_ADMIN_ROOT;?>introImage/list_introImage.php" class="btn btn-info" role="button">檢視列表</a></p>
        </div>
      </div>
    </div>
    <!--  -->
     <div class="col-lg-3 col-md-3">
      <div class="thumbnail">
        <div class="caption">
          <h3>首頁下方圖片修訂</h3>
          <p>網站首頁下方三張圖片、連結位置修訂。</p>
          <hr> 
          <p><a href="<?php echo SITE_ADMIN_ROOT;?>indexImage/list_indexImage.php" class="btn btn-info" role="button">檢視列表</a></p>
        </div>
      </div>
    </div>
    <!--  -->
    <div class="col-lg-3 col-md-3">
      <div class="thumbnail">
        <div class="caption">
          <h3>產品分類標籤修訂</h3>
          <p>建立產品分類，在建立商品時需要選擇是在哪個分類裡。</p>
          <hr> 
          <p><a href="<?php echo SITE_ADMIN_ROOT;?>productTags/add_productTags.php" class="btn btn-warning" role="button">新增標籤</a> <a href="<?php echo SITE_ADMIN_ROOT;?>productTags/list_productTags.php" class="btn btn-info" role="button">檢視列表</a></p>
        </div>
      </div>
    </div>
    <!--  -->
    <div class="col-lg-3 col-md-3">
      <div class="thumbnail">
        <div class="caption">
          <h3>產品</h3>
          <p>建立商品、價格、顯示圖片等。</p>
          <hr> 
          <p><a href="<?php echo SITE_ADMIN_ROOT;?>product/add_product.php" class="btn btn-warning" role="button">新增內容</a> <a href="<?php echo SITE_ADMIN_ROOT;?>product/list_product.php" class="btn btn-info" role="button">檢視列表</a></p>
        </div>
      </div>
    </div>
   
  </div>
</div>
<?php
  //載入Footer
  require 'view/tp_siteFooter.php';
?>