<?php
  //載入網站基本設定
  require 'include/config.php';
  require 'view/tp_siteHeader.php';
  require 'view/tp_header.php';
?>
<style>
  /* line 1, ../sass/siteCss.scss */
.form-signin {
  max-width: 330px;
  padding: 15px;
  margin: 0 auto;
}
/* line 5, ../sass/siteCss.scss */
.form-signin input {
  margin-bottom: 15px;
}

</style>
<div class="container">
  <form class="form-signin" role="form" action="action/checkUser.php" method="post" accept-charset="utf-8">
    <h3 class="form-signin-heading">請先登入</h3>
    <input type="text" class="form-control" placeholder="User name" id="username" name="username" required autofocus>
    <input type="password" class="form-control" placeholder="Password" id="password" name="password" required>
    <button type="submit" class="btn btn-default btn-block">sing in</button>
  </form>
</div>
<?php
  //載入Footer
  require 'view/tp_siteFooter.php';
?>