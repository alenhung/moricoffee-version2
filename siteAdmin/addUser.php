<?php

  require 'include/config.php';
  require 'view/tp_siteHeader.php';
  require 'view/tp_header.php';
  
?>
<style>
  #loginForm{
    max-width: 330px;
    margin: 0 auto;
    padding-bottom: 30px
  }
  input{
    margin-bottom: 15px;
  }
</style>
<form action="action/registerUser.php" id="loginForm" class="form-horizontal" action='' method="POST" role="form">
  <h2>register new user</h2>
  <!-- Username -->
  <input type="text" class="form-control" id="username" name="username" placeholder="username" required autofocus>
  <!-- PssWord -->
  <input type="password" class="form-control" id="password" name="password" placeholder="password" required>
  <input type="password" class="form-control" id="password2" name="password2" placeholder="password again" required>
  
  <button type="submit" class="btn btn-default btn-block">register</button>
</form>
</div>
<!--CONTENT END-->
<?php
  require 'view/tp_siteFooter.php';
?>