<?php
    
    require "../include/connect/DB_connect.php";
    require "../include/config.php";
    require "../include/do_function.php";
    $userTB           =     'userTB';
    $introImageTB     =     'introImage';
    $productTagsTB    =     'productTags';
    $productTB        =     'product';
    $indexImageTB     =     'indexImage';
   //
   date_default_timezone_set('Asia/Taipei');
   $createDate = date ("Y-m-d H:i:s" , mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y')));
   $user_ip = $_SERVER["REMOTE_ADDR"];
   
  define(REGISTER_SUCCESS,"註冊成功！感謝您！");
  define(REGISTER_ERROR,"註冊失敗！請檢查兩次密碼是否吻合！");
  define(SINGIN_SUCCESS,"登入成功！感謝您！");
  define(SINGIN_ERROR,"登入失敗！請檢查使用者名稱或密碼。");
  define(ADD_SUCCESS,"新增成功！感謝您！");
  define(EDIT_SUCCESS,"修改成功！感謝您！");
  define(DELETE_SUCCESS,"刪除成功！感謝您！");
  define(LOGOUT_SUCCESS,"您已成功登出管理系統！");
?>