<?php
//載入基本設定
include_once('../include/config.php');
include_once('../include/do_function.php');
include_once('../include/connect/DB_connect.php');
include_once('config.php');
?>
<meta charset="utf-8">
<?php
  switch ($type) {
  /*********************首頁大圖**********************/
    case AddIntroImage:
      $fileDateName = date ("YmdHis" , mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y')));
      //
      $filename = $_FILES['file']['name'];
      //取得附檔名
      $extend = strtolower(strrchr($filename,'.'));
      //日期加上附檔名
      $fname = 'intro_'.$fileDateName.$extend;
      // 複製上傳圖片到指定 images 目錄
      copy($_FILES['file']['tmp_name'], "../../uploads/" . $fname);
      $upLoadImageUrl = SITE_ROOT.'uploads/'.$fname;
      $sql = "INSERT INTO $introImageTB (imageTitle,imageURL,linkURL,enable,date,ip)
          values('$imageTitle','$upLoadImageUrl','$linkURL','$enableImage','$createDate','$user_ip')";
      mysql_query($sql,$link_ID)
        or die(mysql_error());
      echo "<script>alert('".ADD_SUCCESS."');
           location.href='".SITE_ADMIN_ROOT."introImage/list_introImage.php';
          </script>";
      break;
    case EditIntroImage:
      $fileDateName = date ("YmdHis" , mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y')));
      //
      if($_FILES['file']['size']>0){
      $filename = $_FILES['file']['name'];
      //取得附檔名
        $extend = strtolower(strrchr($filename,'.'));
        //日期加上附檔名
        $fname = "intro_".$fileDateName.$extend;
        // 複製上傳圖片到指定 images 目錄
        copy($_FILES['file']['tmp_name'], "../../uploads/" . $fname);
        $updateImageUrl = SITE_ROOT.'uploads/'.$fname;
      }else{
        $updateImageUrl= $oldImageUrl;
      }
      $sql = "UPDATE $introImageTB SET imageTitle='$imageTitle',imageURL='$updateImageUrl' ,linkURL='$linkURL',enable='$enableImage',date='$createDate',ip='$user_ip' WHERE ID=$id";
      mysql_query($sql,$link_ID)
        or die(mysql_error());
      echo "<script>alert('".EDIT_SUCCESS."');
           location.href='".SITE_ADMIN_ROOT."introImage/list_introImage.php';
          </script>";
      break;
    case DeleteIntroImage:
      $sql = "DELETE FROM $introImageTB WHERE ID='$CAD'";
      mysql_query($sql, $link_ID)
        or die(mysql_error());
      echo "<script>alert('".DELETE_SUCCESS."');
           location.href='".SITE_ADMIN_ROOT."introImage/list_introImage.php';
          </script>";
      break;
  /*********************產品標籤**********************/
    case AddProductTags:
      $sql = "INSERT INTO $productTagsTB (tagName,linkURL,date,ip)
          values('$tagName','$linkURL','$createDate','$user_ip')";
      mysql_query($sql,$link_ID)
        or die(mysql_error());
      echo "<script>alert('".ADD_SUCCESS."');
           location.href='".SITE_ADMIN_ROOT."productTags/list_productTags.php';
          </script>";
      break;
    case EditProductTags:
      $sql = "UPDATE $productTagsTB SET tagName='$tagName', linkURL='$linkURL', date='$createDate',ip='$user_ip' WHERE ID=$id";
      mysql_query($sql,$link_ID)
        or die(mysql_error());
      echo "<script>alert('".EDIT_SUCCESS."');
           location.href='".SITE_ADMIN_ROOT."productTags/list_productTags.php';
          </script>";
      break;
    case DeleteProductTags:
      $sql = "DELETE FROM $productTagsTB WHERE ID='$CAD'";
      mysql_query($sql, $link_ID)
        or die(mysql_error());
      echo "<script>alert('".DELETE_SUCCESS."');
           location.href='".SITE_ADMIN_ROOT."productTags/list_productTags.php';
          </script>";
      break;
  /*********************產品圖片**********************/
    case AddProduct:
      $fileDateName = date ("YmdHis" , mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y')));
      //
      $filename = $_FILES['file']['name'];
      //取得附檔名
      $extend = strtolower(strrchr($filename,'.'));
      //日期加上附檔名
      $fname = 'product_'.$fileDateName.$extend;
      // 複製上傳圖片到指定 images 目錄
      copy($_FILES['file']['tmp_name'], "../../uploads/" . $fname);
      $upLoadImageUrl = SITE_ROOT.'uploads/'.$fname;
      //多選的標籤陣列拆解成','號分開
      $selectTages = implode (",", $selectpicker);
      $sql = "INSERT INTO $productTB (productName,productImage,price,description,tags,enable,date,ip)
          values('$productName','$upLoadImageUrl','$productPrice','$productDescription','$selectTages','$enableProduct','$createDate','$user_ip')";
      mysql_query($sql,$link_ID)
        or die(mysql_error());
      echo "<script>alert('".ADD_SUCCESS."');
           location.href='".SITE_ADMIN_ROOT."product/list_product.php';
          </script>";
      break;
      case EditProduct:
      $fileDateName = date ("YmdHis" , mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y')));
      //
      if($_FILES['file']['size']>0){
      $filename = $_FILES['file']['name'];
      //取得附檔名
        $extend = strtolower(strrchr($filename,'.'));
        //日期加上附檔名
        $fname = "product_".$fileDateName.$extend;
        // 複製上傳圖片到指定 images 目錄
        copy($_FILES['file']['tmp_name'], "../../uploads/" . $fname);
        $updateImageUrl = SITE_ROOT.'uploads/'.$fname;
      }else{
        $updateImageUrl= $oldImageUrl;
      }
      $selectTages = implode (",", $selectpicker);
      $sql = "UPDATE $productTB SET productName='$productName',productImage='$updateImageUrl',price='$productPrice',description='$productDescription',tags='$selectTages',enable='$enableProduct',date='$createDate',ip='$user_ip' WHERE ID=$id";
      mysql_query($sql,$link_ID)
        or die(mysql_error());
      echo "<script>alert('".EDIT_SUCCESS."');
           location.href='".SITE_ADMIN_ROOT."product/list_product.php';
          </script>";
      break;
    case DeleteProduct:
      $sql = "DELETE FROM $productTB WHERE ID='$CAD'";
      mysql_query($sql, $link_ID)
        or die(mysql_error());
      echo "<script>alert('".DELETE_SUCCESS."');
           location.href='".SITE_ADMIN_ROOT."product/list_product.php';
          </script>";
      break;
   /*********************首頁大圖**********************/
    case AddIndexImage:
      $fileDateName = date ("YmdHis" , mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y')));
      //
      $filename = $_FILES['file']['name'];
      //取得附檔名
      $extend = strtolower(strrchr($filename,'.'));
      //日期加上附檔名
      $fname = 'index_'.$fileDateName.$extend;
      // 複製上傳圖片到指定 images 目錄
      copy($_FILES['file']['tmp_name'], "../../uploads/" . $fname);
      $upLoadImageUrl = SITE_ROOT.'uploads/'.$fname;
      $sql = "INSERT INTO $indexImageTB (imageTitle,imageURL,linkURL,date,ip)
          values('$imageTitle','$upLoadImageUrl','$linkURL','$createDate','$user_ip')";
      mysql_query($sql,$link_ID)
        or die(mysql_error());
      echo "<script>alert('".ADD_SUCCESS."');
           location.href='".SITE_ADMIN_ROOT."indexImage/list_indexImage.php';
          </script>";
      break;
    case EditIndexImage:
      $fileDateName = date ("YmdHis" , mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y')));
      //
      if($_FILES['file']['size']>0){
      $filename = $_FILES['file']['name'];
      //取得附檔名
        $extend = strtolower(strrchr($filename,'.'));
        //日期加上附檔名
        $fname = "index_".$fileDateName.$extend;
        // 複製上傳圖片到指定 images 目錄
        copy($_FILES['file']['tmp_name'], "../../uploads/" . $fname);
        $updateImageUrl = SITE_ROOT.'uploads/'.$fname;
      }else{
        $updateImageUrl= $oldImageUrl;
      }
      $sql = "UPDATE $indexImageTB SET imageTitle='$imageTitle',imageURL='$updateImageUrl' ,linkURL='$linkURL',date='$createDate',ip='$user_ip' WHERE ID=$id";
      mysql_query($sql,$link_ID)
        or die(mysql_error());
      echo "<script>alert('".EDIT_SUCCESS."');
           location.href='".SITE_ADMIN_ROOT."indexImage/list_indexImage.php';
          </script>";
      break;
    case DeleteIndexImage:
      $sql = "DELETE FROM $indexImageTB WHERE ID='$CAD'";
      mysql_query($sql, $link_ID)
        or die(mysql_error());
      echo "<script>alert('".DELETE_SUCCESS."');
           location.href='".SITE_ADMIN_ROOT."indexImage/list_indexImage.php';
          </script>";
      break;
    default:
      
      break;
    }
?>