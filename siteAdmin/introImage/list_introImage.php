<?php
   session_start();
  $type=introImage_list;
  //載入網站基本設定
  require '../include/config.php';
  require '../include/check_session.php';
  require '../view/tp_siteHeader.php';
  require '../view/tp_header.php';
  require 'introImage_sql.inc.php';
?>
<style>
  table{
    margin: 40px;
  }
  td{
    padding-top: 10px;
    padding-bottom: 10px;
  }
  button{
    margin-top: 5px;
  }
</style>

<div class="container">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="page-header">
        <h4>網站管理後台 - 官網首頁大圖管理</h4>
      </div>
      <ul class="nav nav-pills">
        <li><a href="add_introImage.php">新增首頁大圖</a></li>
        <li class="active"><a href="list_introImage.php">首頁大圖列表</a></li>
      </ul>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <table class="table">
        <thead>
          <tr>
            <td >ID</td>
            <td >圖片標題</td>
            <td >連結頁面</td>
            <td >目前狀態</td>
            <td >圖片</td>
            <td width="140px">修改/刪除</td>
          </tr>
        </thead>
        <tbody>
          <?php while ($list = mysql_fetch_array($result)) { ?>
          <tr>
            <td><?php echo $list['id'];?></td>
            <td><?php echo $list['imageTitle'];?></td>
            <td><a href="<?php echo $list['linkURL'];?>" target="_blank">連接網頁</a></td>
            <td>
              <?php if($list['enable'] == true){
                 echo '<p><span class="label label-success">顯示</span></p>';
                }else{

                 echo '<p><span class="label label-default">隱藏</span></p>';
                  } ;?>
            </td>
            <td>
              <a href="<?php echo $list['imageURL'] ;?>" target="_blank" data-lightbox="<?php echo $list['imageURL'] ;?>" title="<?php echo $list['imageTitle'] ;?>"><img src="<?php echo $list['imageURL'] ;?>" alt="" width="100"></a>
            </td>
            <td>
              <a href="<?php echo SITE_ADMIN_ROOT;?>introImage/edit_introImage.php?CAD=<?php echo $list['id'] ;?>" type="button" class="btn btn-info btn-xs">修改</a>
              <a href="<?php echo SITE_ADMIN_ROOT;?>action/modify.php?type=DeleteIntroImage&CAD=<?php echo $list['id'] ;?>" type="button" class="btn btn-danger btn-xs">刪除</a>

            </td>
          
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>  
</div>
<?php
  //載入Footer
  require '../view/tp_siteFooter.php';
?>
<script src="<?php echo SITE_ADMIN_ROOT ;?>plugin/lightbox/js/lightbox-2.6.min.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="<?php echo SITE_ADMIN_ROOT ;?>plugin/lightbox/css/lightbox.css">
<script>
  
</script>