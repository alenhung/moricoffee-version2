<?php
  session_start();
  //載入網站基本設定
  require '../include/config.php';
  require '../include/check_session.php';
  require '../view/tp_siteHeader.php';
  require '../view/tp_header.php';
?>
<div class="container">
  <div class="row">
    <div class="col-lg-12">
      <div class="page-header">
        <h4>網站管理後台 - 官網首頁圖片列表管理</h4>
      </div>
      <ul class="nav nav-pills">
<!--         <li class="active"><a href="add_indexImage.php">新增首頁大圖</a></li> -->
        <li><a href="list_indexImage.php">首頁圖片列表</a></li>
      </ul>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
      <form class="form-horizontal" role="form" action="../action/modify.php?type=AddIndexImage" method="post" enctype="multipart/form-data">
        <div class="form-group">
          <label for="imageTitle" class="col-sm-4 control-label">圖片標題</label>
          <div class="col-sm-8">
            <input type="text" class="form-control" name="imageTitle" id="imageTitle" placeholder="圖片標題">
          </div>
        </div>
        <div class="form-group">
          <label for="linkURL" class="col-sm-4 control-label">連結頁面</label>
          <div class="col-sm-8">
            <input type="text" class="form-control" name="linkURL" id="linkURL" placeholder="連結網址">
          </div>
        </div>
        <div class="form-group">
          <label for="indexImage" class="col-sm-4 control-label">圖片</label>
          <div class="col-sm-8">
          <input type="file" class="form-control" id="indexImage" name="file" > 
          </div>
        </div>
        <button type="submit" class="btn btn-primary pull-right">新增</button>
      </form>
    </div>
  </div>
</div>
<?php
  //載入Footer
  require '../view/tp_siteFooter.php';
?>