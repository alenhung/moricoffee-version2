<?php
   session_start();
  $type=indexImage_edit;
    //載入網站基本設定
  require '../include/config.php';
  require '../include/check_session.php';
  require '../view/tp_siteHeader.php';
  require '../view/tp_header.php';
  require 'indexImage_sql.inc.php';
?>

<div class="container">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="page-header">
        <h4>網站管理後台 - 官網首頁圖片列表管理</h4>
      </div>
      <ul class="nav nav-pills">
<!--         <li ><a href="add_indexImage.php">新增首頁大圖</a></li> -->
        <li><a href="list_indexImage.php">首頁圖片列表</a></li>
      </ul>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
      <form class="form-horizontal" role="form" action="../action/modify.php?type=EditIndexImage" method="post" enctype="multipart/form-data">
        <div class="form-group">
          <label for="imageTitle" class="col-sm-4 control-label">圖片標題</label>
          <div class="col-sm-8">
            <input type="text" class="form-control" name="imageTitle" id="imageTitle" value="<?php echo $list['imageTitle'] ;?>">
          </div>
        </div>
        <div class="form-group">
          <label for="imageTitle" class="col-sm-4 control-label">連結頁面</label>
          <div class="col-sm-8">
            <input type="text" class="form-control" name="linkURL" id="linkURL" value="<?php echo $list['linkURL'] ;?>">
          </div>
        </div>
        <div class="form-group">
          <label for="indexImage" class="col-sm-4 control-label">選擇圖片</label>
          <div class="col-sm-8">
          <input type="file" class="form-control" id="indexImage" name="file" >
          <input type="hidden" class="form-control" id="oldImageUrl" name="oldImageUrl" value="<?php echo $list['imageURL'] ;?>">
          <input type="hidden" class="form-control" name="id" id="id" value="<?php echo $list['id'] ;?>">
          
          </div>
        </div>
        <button type="submit" class="btn btn-primary pull-right">修改</button>
      </form>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
      <p>目前圖片：</p>
            <h4>如果圖片沒更新就不用上傳圖片！</h4>
            <a href="<?php echo $list['imageURL'] ;?>" target="_blank" data-lightbox="<?php echo $list['imageURL'] ;?>" title="<?php echo $list['imageTitle'] ;?>"><img src="<?php echo $list['imageURL'] ;?>" alt="" class="img-responsive"></a>
    </div>
  </div>
</div>
<?php
  //載入Footer
  require '../view/tp_siteFooter.php';
?>
<script src="<?php echo SITE_ADMIN_ROOT ;?>plugin/lightbox/js/lightbox-2.6.min.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="<?php echo SITE_ADMIN_ROOT ;?>plugin/lightbox/css/lightbox.css">
<script>
  