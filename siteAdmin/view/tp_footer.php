<footer>
  <div class="container">
    <div class="row">
      <div class="col-log-5 col-md-5 col-sm-12 col-xs-12">
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 footer-top-words text-center">
            <span class="title2"><a href="#">THE SHOWROOM</a></span>
          </div> 
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 footer-top-words text-center"> 
            <span class="title2"><a href="#">Our Blog</a></span>
          </div>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
            <hr class="hr-Type"/>
          </div>
          <div class="col-3 col-md-3 col-sm-3 col-xs-12 footer-link">
            <p><a href="#">About Us</a></p>
          </div>
          <div class="col-2 col-md-2 col-sm-2 col-xs-12 footer-link">
            <p><a href="#">Contact</a></p>
          </div>
          <div class="col-2 col-md-2 col-sm-2 col-xs-12 footer-link">
            <p><a href="#">Shopping</a></p>
          </div>
          <div class="col-5 col-md-5 col-sm-5 col-xs-12 footer-link">
            <p><a href="#">Privacy & Security</a></p>
          </div>
        </div>
      </div>
      <div class="col-log-5 col-md-2 col-sm-12 col-xs-12 ">
        <img src="<?php echo SITE_ROOT;?>img/foot-logo.gif" alt="moricoffee" class="img-responsive ">
      </div>
      <div class="col-log-5 col-md-5 col-sm-12 col-xs-12">
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 footer-top-words text-center"> 
            <span class="title2"><a href="#">MEET THE TEAM</a></span>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 footer-top-words text-center"> 
            <span class="title2"><a href="#">KNOWLEDGE BASE</a></span>
          </div>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
            <hr class="hr-Type"/>
          </div>
          <div class="col-12 col-md-12 col-sm-12 col-xs-12 footer-link">
            <p><span class="greenText">Start the Conversation:</span>  +886-2-2208-3266 | <a href="mailto:sales@moricoffee.com">sales@moricoffee.com</a></p>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>