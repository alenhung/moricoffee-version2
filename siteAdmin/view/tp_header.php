<header>
  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <h3><?php echo SITE_NAME;?> - 後端管理</h3>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div id="header-control" class="pull-right">
          <ul class="list-inline">
            <li><a href="<?php echo SITE_ADMIN_ROOT;?>siteMain.php">返回主選單</a></li>
            <li><a href="mailto:alenhung@gmail.com">問題回報</a></li>
            <li><a class="btn btn-success"  role="button" href="<?php echo SITE_ADMIN_ROOT;?>action/logout.php">登出</a></li>
          </ul>
        </div>
      </div>
    </div>
    <hr/>
</header>
