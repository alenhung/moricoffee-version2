<?php
  session_start();
  $type=product_Add;
  //載入網站基本設定
  require '../include/config.php';
  require '../include/check_session.php';
  require '../view/tp_siteHeader.php';
  require '../view/tp_header.php';
  require 'product_sql.inc.php';
?>
<div class="container">
  <div class="row">
    <div class="col-lg-12">
      <div class="page-header">
        <h4>網站管理後台 - 產品管理</h4>
      </div>
      <ul class="nav nav-pills">
        <li class="active"><a href="add_product.php">新增產品</a></li>
        <li><a href="list_product.php">產品列表</a></li>
      </ul>
    </div>
    <form id="fileupload" class="form-horizontal" role="form" action="../action/modify.php?type=AddProduct" method="post" enctype="multipart/form-data">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
      
        <div class="form-group">
          <label for="productName" class="col-sm-4 control-label">產品名稱</label>
          <div class="col-sm-8">
            <input type="text" class="form-control" name="productName" id="productName" placeholder="產品標籤名稱">
          </div>
        </div>
        <div class="form-group">
          <label for="productPrice" class="col-sm-4 control-label">產品價格</label>
          <div class="col-sm-8">
            <input type="text" class="form-control" name="productPrice" id="productPrice" placeholder="產品標籤名稱">
          </div>
        </div>
        <div class="form-group">
          <label for="productDescription" class="col-sm-4 control-label">產品描述</label>
          <div class="col-sm-8">
            <textarea class="form-control" id="productDescription" name="productDescription" rows="3"></textarea>
          </div>
        </div>
        <!---->
         <div class="form-group">
          <label for="tageArray" class="col-sm-4 control-label">產品相關標籤</label>
          <div class="col-sm-8">    
            <?php while ($list = mysql_fetch_array($result)) { ?>
            <label class="checkbox-inline">
              <input type="checkbox" id="inlineCheckbox1" name="selectpicker[]" value="<?php echo $list['id'];?>"> <?php echo $list['tagName'];?>
            </label>
            <?php } ?>
          </div>
        </div>
        <!--  -->
        <div class="form-group">
          <label for="enableProduct" class="col-sm-4 control-label">是否上架</label>
          <div class="col-sm-8">
            <div class="radio">
              <label>
                <input type="radio" name="enableProduct" id="enableProduct1" value="0" >
                隱藏
              </label>
            </div>
            <div class="radio">
              <label>
                <input type="radio" name="enableProduct" id="enableProduct2" value="1" checked>
                顯示
              </label>
            </div>
          </div>
        </div>
        <!--  -->
        <!---->
        <div class="form-group">
          <label for="productImage" class="col-sm-4 control-label">產品主要圖片</label>
          <div class="col-sm-8">
          <input type="file" class="form-control" id="productImage" name="file" > 
          </div>
        </div>
        <!--  -->
        <button type="submit" class="btn btn-primary pull-right">新增</button>
      
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

    </div>
    </form>
  </div>
</div>
<?php
  //載入Footer
  require '../view/tp_siteFooter.php';
?>