<?php
  session_start();
  $type=product_edit;
  //載入網站基本設定
  require '../include/config.php';
  require '../include/check_session.php';
  require '../view/tp_siteHeader.php';
  require '../view/tp_header.php';
  require 'product_sql.inc.php';
?>
<div class="container">
  <div class="row">
    <div class="col-lg-12">
      <div class="page-header">
        <h4>網站管理後台 - 產品管理</h4>
      </div>
      <ul class="nav nav-pills">
        <li><a href="add_product.php">新增產品</a></li>
        <li><a href="list_product.php">產品列表</a></li>
      </ul>
    </div>
    <form id="fileupload" class="form-horizontal" role="form" action="../action/modify.php?type=EditProduct" method="post" enctype="multipart/form-data">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
      
        <div class="form-group">
          <label for="productName" class="col-sm-4 control-label">產品名稱</label>
          <div class="col-sm-8">
            <input type="text" class="form-control" name="productName" id="productName" value="<?php echo $list['productName'];?>">
          </div>
        </div>
        <div class="form-group">
          <label for="productPrice" class="col-sm-4 control-label">產品價格</label>
          <div class="col-sm-8">
            <input type="text" class="form-control" name="productPrice" id="productPrice" value="<?php echo $list['price'];?>">
          </div>
        </div>
        <div class="form-group">
          <label for="productDescription" class="col-sm-4 control-label">產品描述</label>
          <div class="col-sm-8">
            <textarea class="form-control" id="productDescription" name="productDescription" rows="3">
            <?php echo $list['description'];?>
            </textarea>
          </div>
        </div>
        <!---->
         <div class="form-group">
          <label for="tageArray" class="col-sm-4 control-label">產品相關標籤</label>
          <div class="col-sm-8">    
            <?php while ($list2 = mysql_fetch_array($result2)) { ?>
              <label class="checkbox-inline">
                <?php
                  $tagsArray = explode(",",$list['tags']);
                  if(in_array($list2['id'], $tagsArray)){
                    echo '<input type="checkbox" id="inlineCheckbox1" name="selectpicker[]" value="'.$list2['id'].'" checked>'.$list2['tagName'];
                  }else{
                    echo '<input type="checkbox" id="inlineCheckbox1" name="selectpicker[]"  value="'.$list2['id'].'" >'.$list2['tagName'];
                  }
                ?>
                </label>
              <?php } ?>
    
          </div>
        </div>
        <!--  -->
        <div class="form-group">
          <label for="enableProduct" class="col-sm-4 control-label">是否上架</label>
          <div class="col-sm-8">
            <?php 
            if($list['enable'] == 1){
            ?>
            <div class="radio">
              <label>
                <input type="radio" name="enableProduct" id="enableProduct1" value="0" >下架
              </label>
            </div>
            <div class="radio">
              <label>
                <input type="radio" name="enableProduct" id="enableProduct2" value="1" checked>上架
              </label>
            </div>
          <?php
            }else{
          ?>
            <div class="radio">
              <label>
                <input type="radio" name="enableProduct" id="enableProduct1" value="0" checked>下架
              </label>
            </div>
            <div class="radio">
              <label>
                <input type="radio" name="enableProduct" id="enableProduct2" value="1">上架
              </label>
            </div>
          <?php
          }
            ?>
          </div>
        </div>
        <!--  -->
        <!---->
        <div class="form-group">
          <label for="productImage" class="col-sm-4 control-label">產品主要圖片</label>
          <div class="col-sm-8">
          <input type="file" class="form-control" id="productImage" name="file" > 
          <input type="hidden" class="form-control" id="oldImageUrl" name="oldImageUrl" value="<?php echo $list['productImage'] ;?>">
          <input type="hidden" class="form-control" name="id" id="id" value="<?php echo $list['id'] ;?>">
          </div>
        </div>
        <!--  -->
        <button type="submit" class="btn btn-primary pull-right">修改</button>
      
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
      <p>目前圖片：</p>
      <a href="<?php echo $list['imageURL'] ;?>" target="_blank" data-lightbox="<?php echo $list['productImage'] ;?>" title="<?php echo $list['productName'] ;?>"><img src="<?php echo $list['productImage'] ;?>" alt="" class="img-responsive"></a>
      <h4>如果圖片沒更新就不用上傳圖片！</h4>
    </div>
    </form>
  </div>
</div>
<?php
  //載入Footer
  require '../view/tp_siteFooter.php';
?>