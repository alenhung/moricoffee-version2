<?php
   session_start();
  $type=product_list;
  //載入網站基本設定
  require '../include/config.php';
  require '../include/check_session.php';
  require '../view/tp_siteHeader.php';
  require '../view/tp_header.php';
  require 'product_sql.inc.php';
?>
<div class="container">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="page-header">
        <h4>網站管理後台 - 產品管理</h4>
      </div>
      <ul class="nav nav-pills">
        <li><a href="add_product.php">新增產品</a></li>
        <li class="active"><a href="list_product.php">產品列表</a></li>
      </ul>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <table class="table">
        <thead>
          <tr>
            <td >ID</td>
            <td width="100px">產品名稱</td>
            <td >價格</td>
            <td width="400px">產品描述</td>
            <td width="100px">產品標籤</td>
            <td width="100px">產品上下架</td>
            <td >產品代表圖片</td>
            <td width="140px">修改/刪除</td>
          </tr>
        </thead>
        <tbody>
          <?php while ($list = mysql_fetch_array($result)) { ?>
          <tr>
            <td><?php echo $list['id'];?></td>
            <td><?php echo $list['productName'];?></td>
            <td><?php echo $list['price'];?></td>
            <td>
                <div class="smallcontent">
                  <?php echo mb_substr($list['description'],0,30,"UTF-8");?>... <a href="#" class="showAllContent">詳細內容</a>
                </div>
                
                <div class="allcontent">
                  <?php echo nl2br($list['description']);?>
                  <a href="#" class="hideallContent">簡短內容</a>
                </div>
            </td>
            <td>
              <?php
                $tages = $list['tags'];
                $str2 = "SELECT * FROM $productTagsTB WHERE id IN ($tages)";
                $result2 = mysql_query($str2,$link_ID);
                while ($list2 = mysql_fetch_array($result2)) {
                  ?>
                  <button class="btn btn-default btn-xs">
                  <?php
                    echo $list2['tagName'];
                  ?>
                  </button>
              <?php    
                }
              ?>
            </td>
            <td>
              <?php if($list['enable'] == true){
                 echo '<p><span class="label label-success">上架中</span></p>';
                }else{

                 echo '<p><span class="label label-danger">下架</span></p>';
                  } ;?>
            </td>
            <td>
              <a href="<?php echo $list['productImage'] ;?>" target="_blank" data-lightbox="<?php echo $list['productImage'] ;?>" title="<?php echo $list['productName'] ;?>"><img src="<?php echo $list['productImage'] ;?>" alt="" width="100"></a>
            </td>
            <td>
              <a href="<?php echo SITE_ADMIN_ROOT;?>product/edit_product.php?CAD=<?php echo $list['id'] ;?>" type="button" class="btn btn-info btn-xs">修改</a>
              <a href="<?php echo SITE_ADMIN_ROOT;?>action/modify.php?type=DeleteProduct&CAD=<?php echo $list['id'] ;?>" type="button" class="btn btn-danger btn-xs">刪除</a>
            </td>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>  
</div>
<?php
  //載入Footer
  require '../view/tp_siteFooter.php';
?>
<script src="<?php echo SITE_ADMIN_ROOT ;?>plugin/lightbox/js/lightbox-2.6.min.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="<?php echo SITE_ADMIN_ROOT ;?>plugin/lightbox/css/lightbox.css">
<script>
  $(document).ready(function() {
     $('.allcontent').hide();
     $('.showAllContent').each(function(index) {
       $(this).click(function(event) {
        $('.allcontent').eq(index).show();
        $('.smallcontent').eq(index).hide();
       });
     });
     $('.hideallContent').each(function(index) {
       $(this).click(function(event) {
         $('.allcontent').eq(index).hide();
        $('.smallcontent').eq(index).show();
       });
     });
  });
</script>