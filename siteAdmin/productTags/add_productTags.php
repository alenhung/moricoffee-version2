<?php
  session_start();
  //載入網站基本設定
  require '../include/config.php';
  require '../include/check_session.php';
  require '../view/tp_siteHeader.php';
  require '../view/tp_header.php';
?>
<div class="container">
  <div class="row">
    <div class="col-lg-12">
      <div class="page-header">
        <h4>網站管理後台 - 產品分類標籤管理</h4>
      </div>
      <ul class="nav nav-pills">
        <li class="active"><a href="add_productTags.php">新增產品分類標籤</a></li>
        <li><a href="list_productTags.php">產品分類標籤列表</a></li>
      </ul>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
      <form class="form-horizontal" role="form" action="../action/modify.php?type=AddProductTags" method="post" enctype="multipart/form-data">
        <div class="form-group">
          <label for="tagName" class="col-sm-4 control-label">產品標籤名稱</label>
          <div class="col-sm-8">
            <input type="text" class="form-control" name="tagName" id="tagName" placeholder="產品標籤名稱">
          </div>
        </div>
        <div class="form-group">
          <label for="linkURL" class="col-sm-4 control-label">連結頁面</label>
          <div class="col-sm-8">
            <input type="text" class="form-control" name="linkURL" id="linkURL" placeholder="連結網址">
          </div>
        </div>
        <button type="submit" class="btn btn-primary pull-right">新增</button>
      </form>
    </div>
  </div>
</div>
<?php
  //載入Footer
  require '../view/tp_siteFooter.php';
?>