<?php
   session_start();
  $type=productTags_edit;
    //載入網站基本設定
  require '../include/config.php';
  require '../include/check_session.php';
  require '../view/tp_siteHeader.php';
  require '../view/tp_header.php';
  require 'productTags_sql.inc.php';
?>

<div class="container">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="page-header">
        <h4>網站管理後台 - 官網首頁大圖管理</h4>
      </div>
      <ul class="nav nav-pills">
        <li><a href="add_productTags.php">新增產品分類標籤</a></li>
        <li><a href="list_productTags.php">產品分類標籤列表</a></li>
      </ul>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
      <form class="form-horizontal" role="form" action="../action/modify.php?type=EditProductTags" method="post">
        <div class="form-group">
          <label for="tagName" class="col-sm-4 control-label">圖片標題</label>
          <div class="col-sm-8">
            <input type="text" class="form-control" name="tagName" id="tagName" value="<?php echo $list['tagName'] ;?>">
          </div>
        </div>
        <div class="form-group">
          <label for="linkURL" class="col-sm-4 control-label">連結頁面</label>
          <div class="col-sm-8">
            <input type="text" class="form-control" name="linkURL" id="linkURL" value="<?php echo $list['linkURL'] ;?>">
            <input type="hidden" class="form-control" name="id" id="id" value="<?php echo $list['id'] ;?>">
          </div>
        </div>
        <button type="submit" class="btn btn-primary pull-right">修改</button>
      </form>
    </div>
  </div>
</div>
<?php
  //載入Footer
  require '../view/tp_siteFooter.php';
?>
<script src="<?php echo SITE_ADMIN_ROOT ;?>plugin/lightbox/js/lightbox-2.6.min.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="<?php echo SITE_ADMIN_ROOT ;?>plugin/lightbox/css/lightbox.css">
<script>
  