<?php
   session_start();
  $type=productTags_list;
  //載入網站基本設定
  require '../include/config.php';
  require '../include/check_session.php';
  require '../view/tp_siteHeader.php';
  require '../view/tp_header.php';
  require 'productTags_sql.inc.php';
?>
<div class="container">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="page-header">
        <h4>網站管理後台 - 官網首頁大圖管理</h4>
      </div>
      <ul class="nav nav-pills">
        <li ><a href="add_productTags.php">新增產品分類標籤</a></li>
        <li class="active"><a href="list_productTags.php">產品分類標籤列表</a></li>
      </ul>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <table class="table">
        <thead>
          <tr>
            <td >ID</td>
            <td >標籤名稱</td>
            <td >連結頁面</td>
            <td width="140px">修改/刪除</td>
          </tr>
        </thead>
        <tbody>
          <?php while ($list = mysql_fetch_array($result)) { ?>
          <tr>
            <td><?php echo $list['id'];?></td>
            <td><?php echo $list['tagName'];?></td>
            <td><a href="<?php echo $list['linkURL'];?>" target="_blank">連接網頁</a></td>
            <td>
              <a href="<?php echo SITE_ADMIN_ROOT;?>productTags/edit_productTags.php?CAD=<?php echo $list['id'] ;?>" type="button" class="btn btn-info btn-xs">修改</a>
              <a href="<?php echo SITE_ADMIN_ROOT;?>action/modify.php?type=DeleteProductTags&CAD=<?php echo $list['id'] ;?>" type="button" class="btn btn-danger btn-xs">刪除</a>
            </td>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>  
</div>
<?php
  //載入Footer
  require '../view/tp_siteFooter.php';
?>
<script src="<?php echo SITE_ADMIN_ROOT ;?>plugin/lightbox/js/lightbox-2.6.min.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="<?php echo SITE_ADMIN_ROOT ;?>plugin/lightbox/css/lightbox.css">
<script>
  
</script>