-- phpMyAdmin SQL Dump
-- version 4.0.4.2
-- http://www.phpmyadmin.net
--
-- 主機: localhost
-- 產生日期: 2014 年 05 月 11 日 20:19
-- 伺服器版本: 5.6.12
-- PHP 版本: 5.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 資料庫: `moricoff_db`
--
CREATE DATABASE IF NOT EXISTS `moricoff_db` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `moricoff_db`;

-- --------------------------------------------------------

--
-- 表的結構 `indexImage`
--

CREATE TABLE IF NOT EXISTS `indexImage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imageTitle` varchar(255) NOT NULL,
  `imageURL` varchar(255) NOT NULL,
  `linkURL` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `ip` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- 轉存資料表中的資料 `indexImage`
--

INSERT INTO `indexImage` (`id`, `imageTitle`, `imageURL`, `linkURL`, `date`, `ip`) VALUES
(1, '左側下方圖片', 'http://moricoffee.com/uploads/index_20140512011300.jpg', 'http://google.com.tw', '2014-05-12', 'fe80::1'),
(2, '中間下方圖片', 'http://moricoffee.com/uploads/index_20140512011607.jpg', 'http://google.com.tw', '2014-05-12', 'fe80::1'),
(3, '右側下方圖片', 'http://moricoffee.com/uploads/index_20140512011637.jpg', 'http://google.com.tw', '2014-05-12', 'fe80::1');

-- --------------------------------------------------------

--
-- 表的結構 `introImage`
--

CREATE TABLE IF NOT EXISTS `introImage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imageTitle` varchar(255) NOT NULL,
  `imageURL` varchar(255) NOT NULL,
  `linkURL` varchar(255) NOT NULL,
  `enable` int(11) NOT NULL,
  `date` date NOT NULL,
  `ip` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- 轉存資料表中的資料 `introImage`
--

INSERT INTO `introImage` (`id`, `imageTitle`, `imageURL`, `linkURL`, `enable`, `date`, `ip`) VALUES
(1, 'introImage1', 'http://moricoffee.com/uploads/intro_20140510172835.jpg', 'http://google.com.tw', 1, '2014-05-10', 'fe80::1'),
(2, 'introImage2', 'http://moricoffee.com/uploads/intro_20140510172856.jpg', 'http://facebook.com', 1, '2014-05-10', 'fe80::1');

-- --------------------------------------------------------

--
-- 表的結構 `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productName` varchar(255) NOT NULL,
  `productImage` varchar(255) NOT NULL,
  `price` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `tags` varchar(255) NOT NULL,
  `enable` int(11) NOT NULL,
  `date` date NOT NULL,
  `ip` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- 轉存資料表中的資料 `product`
--

INSERT INTO `product` (`id`, `productName`, `productImage`, `price`, `description`, `tags`, `enable`, `date`, `ip`) VALUES
(1, '咖啡壺', 'http://moricoffee.com/uploads/product_20140512040527.jpg', '1145678', 'Manufactured by our friends over at Hario Japan, this scale is durable, hyper-compact and quite sensitive. It measures in tenths of a gram, includes a built-in timer, and has a maximum capacity of 2 kilos (4.4 pounds). It comes in one color, matte black, which for our money is the best possible color a scale can come in. It takes two AAA battieres, which are included; and its modest dimensions – 7.5" by 4.75" by 1.2" – make it a pleasingly practical option for travel.', '2', 1, '2014-05-12', 'fe80::1'),
(2, '咖啡背包', 'http://moricoffee.com/uploads/product_20140512022016.jpg', '78', 'Late in 2010, our Training Department was tasked with finding an excellent scale. The criteria: It must weigh in tenths of a gram, have a continuous read-out, an effortless tare, long battery life, and very sturdy construction. The AWS is the fruit of their months-long labor. We love how reliable and robust this scale is. We use it for making and teaching pour-over, Chemex and nel drip, weighing espresso shot volume, calibrating espresso grinders, and dozens of other uses where precise amounts of coffee or water are necessary. Its impressively large weight capacity (2,000 grams) makes it a favorite among our egg cracking, cake frosting, dough-weighing pastry extraordinaires, as well.', '3,2', 1, '2014-05-12', 'fe80::1'),
(3, '磨鬥雞', 'http://moricoffee.com/uploads/product_20140512040603.jpg', '1768', 'It''s a corny locution, true, but the Takahiro 0.9 liter kettle is the Rolls Royce of pouring kettles. There is no kettle, in our opinion, that is as silky and responsive to a pour. After a little practice, we got a stream of water that was so slow -- yet fine and unbroken -- that we could see the individual drops of water cohere like we were pouring the tiniest pearl necklace. And the coffee was good, too. Not recommended for direct heating over a gas flame.', '3', 1, '2014-05-12', 'fe80::1'),
(4, '手沖濾紙', 'http://moricoffee.com/uploads/product_20140512021728.jpg', '123', 'These filters are the very cornerstone of the Chemex brew process: A Chemex without them is like a car without a steering wheel. Or like a yo-yo without a string. At risk of running our simile supply into the ground, suffice it to say that these are thick and sturdy, and help produce a clean and exquisite cup.', '3,2,1', 1, '2014-05-12', 'fe80::1'),
(5, '手沖濾紙', 'http://moricoffee.com/uploads/product_20140512021955.jpg', '2332', 'These filters are the very cornerstone of the Chemex brew process: A Chemex without them is like a car without a steering wheel. Or like a yo-yo without a string. At risk of running our simile supply into the ground, suffice it to say that these are thick and sturdy, and help produce a clean and exquisite cup.', '3,2', 1, '2014-05-12', 'fe80::1'),
(6, 'test', 'http://moricoffee.com/uploads/product_20140512040353.jpg', '4564645', 'Our Pastry Chef, Caitlin Williams Freeman, released “Modern Art Desserts” just a few months before the SFMOMA closed its doors for a two-year expansion project. This set includes Caitlin’s critically acclaimed book and a limited edition tote that memorializes our Rooftop Garden café.', '3,2', 1, '2014-05-12', 'fe80::1');

-- --------------------------------------------------------

--
-- 表的結構 `productTags`
--

CREATE TABLE IF NOT EXISTS `productTags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tagName` varchar(255) NOT NULL,
  `linkURL` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `ip` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- 轉存資料表中的資料 `productTags`
--

INSERT INTO `productTags` (`id`, `tagName`, `linkURL`, `date`, `ip`) VALUES
(1, '11test22321', 'test', '2014-05-10', 'fe80::1'),
(2, '標籤1', 'http://moricoffee.com', '2014-05-11', 'fe80::1'),
(3, '咖啡周邊', '', '2014-05-11', 'fe80::1');

-- --------------------------------------------------------

--
-- 表的結構 `userTB`
--

CREATE TABLE IF NOT EXISTS `userTB` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `ip` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- 轉存資料表中的資料 `userTB`
--

INSERT INTO `userTB` (`id`, `username`, `password`, `date`, `ip`) VALUES
(1, 'alenhung', '96ac9a11d94d8f982ba476aa4b5ef503', '2014-05-07', '127.0.0.1');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
