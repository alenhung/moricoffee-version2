<?php
  //載入網站基本設定
  require 'include/config.php';
  require 'view/tp_siteHeader.php';
  require 'view/tp_header.php';

?>
<div class="container">
  <div class="thumbnail">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <?php require 'view/tp_topBlock.php';?>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <img src="<?php echo SITE_ROOT;?>img/fb-bar.jpg" alt="" class="img-responsive" id="fbBar">
        <div id="address" class="graybakcground-left">
          <div class="indexContent">
            <p><span class="title1">CONTACT US</span></p>
            <p><b>Email:</b> <br><a href="mailto:sales@moricoffee.com">sales@moricoffee.com</a></p>
            <p><b>Line ID:</b><br>moricoffee</p>
          </div>
        </div>
      </div>
      <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
        <div class="graybakcground-right">
          <div class="indexContent">
            <p><span class="title1">ESPRESSO Your Life</span></p>
            <p>
              Espresso (/ɛˈspɹɛsəʊ/ italian: /eˈsprɛsso/) is coffee brewed by forcing a small amount of nearly boiling water under pressure through finely ground coffee beans. Espresso is generally thicker than coffee brewed by other methods, has a higher concentration of suspended and dissolved solids, and has crema on top (a foam with a creamy consistency). As a result of the pressurized brewing process, the flavors and chemicals in a typical cup of espresso are very concentrated. Espresso is the base for other drinks, such as a caffè latte, cappuccino, caffè macchiato, cafe mocha, or caffè Americano. Espresso has more caffeine per unit volume than most beverages, but the usual serving size is smaller—a typical 60 mL (2 US fluid ounces) of espresso has 80 to 150 mg of caffeine, a little less than the 95 to 200 mg of a standard 240 mL (8 US fluid ounces) cup of drip-brewed coffee.
            </p>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <?php require 'view/tp_footerimage.php';?>
    </div>
  </div>
</div>
<?php
  require 'view/tp_footer.php';
  require 'view/tp_siteFooter.php';
?>
<?php print_r($list['imageURL']) ;?>