<header>
  <div class="container">
    <div class="row">
      <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
        <a href="<?php echo SITE_ROOT;?>"><img src="<?php echo SITE_ROOT;?>img/logo.gif" alt="Moricoffee" class="img-responsive"></a>
      </div>
      <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
        
        <div id="socialList" class="pull-right">
          <i class=" fa fa-phone"></i> <i class=" fa fa-inbox"></i> <i class=" fa fa-facebook"></i> <i class=" fa fa-instagram"></i> <i class=" fa fa-youtube"></i> 
        </div>
      </div>
    </div>
    <hr class="siteHR" />
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <?php require 'tp_nav.php';?>
      </div>
    </div>
  </div>
</header>
