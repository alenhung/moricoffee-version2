<nav>
  <ul class="list-inline">
    <li><a href="<?php echo SITE_ROOT;?>machines.php">Coffee Makers</a></li>
    <li><a href="<?php echo SITE_ROOT;?>">Grinders & Support Gear</a></li>
    <li><a href="<?php echo SITE_ROOT;?>">Coffee</a></li>
    <li><a href="<?php echo SITE_ROOT;?>">Subscriptions</a></li>
    <li><a href="<?php echo SITE_ROOT;?>">Brewing Book</a></li>
  </ul>
</nav>
